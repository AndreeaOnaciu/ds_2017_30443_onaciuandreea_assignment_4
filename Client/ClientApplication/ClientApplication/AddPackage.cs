﻿using ClientApplication.AdminOperations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class AddPackage : Form
    {
        AdminOperations.AdminServiceClient client = new AdminOperations.AdminServiceClient();

        public AddPackage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminMenu adminMenuForm = new AdminMenu();
            adminMenuForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String senderUser = usernameSenderTB.Text;
            String receiverUser = usernamerReceiverTB.Text;
            String description = descriptionTB.Text;
            String senderCity = senderCityTB.Text;
            String receiverCity = destinationCityTB.Text;
            String packageName = packageNameTB.Text;

            package p = new package();
            p.description = description;
            p.destinationCity = receiverCity;
            p.senderCity = senderCity;
            p.tracking = false;
            p.name = packageName;
            p.routes = null;
            p.sender = client.getUser(senderUser);
            p.receiver = client.getUser(receiverUser);

            client.savePackage(p);
            MessageBox.Show("Package has been saved successfully!");
        }
    }
}
