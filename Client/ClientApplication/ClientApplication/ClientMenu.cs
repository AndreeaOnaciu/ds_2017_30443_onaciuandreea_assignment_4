﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class ClientMenu : Form
    {
        int userId;
        public ClientMenu(int userId)
        {
            this.userId = userId;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login loginForm = new Login();
            loginForm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            SeePackages seePackagesForm = new SeePackages(userId);
            seePackagesForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            SeePackageStatus seePackagesForm = new SeePackageStatus(userId);
            seePackagesForm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            SearchPackage searchPackageForm = new SearchPackage(userId);
            searchPackageForm.Show();
        }
    }
}
