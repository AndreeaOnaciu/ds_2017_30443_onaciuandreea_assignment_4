﻿namespace ClientApplication
{
    partial class UpdateStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Back = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.packageId = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.currentCityTB = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(465, 30);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(127, 29);
            this.Back.TabIndex = 0;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Package Identifier:";
            // 
            // packageId
            // 
            this.packageId.Location = new System.Drawing.Point(211, 90);
            this.packageId.Name = "packageId";
            this.packageId.Size = new System.Drawing.Size(114, 22);
            this.packageId.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(56, 163);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(149, 32);
            this.button1.TabIndex = 3;
            this.button1.Text = "Get Current City";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // currentCityTB
            // 
            this.currentCityTB.Location = new System.Drawing.Point(211, 163);
            this.currentCityTB.Multiline = true;
            this.currentCityTB.Name = "currentCityTB";
            this.currentCityTB.Size = new System.Drawing.Size(143, 32);
            this.currentCityTB.TabIndex = 4;
            this.currentCityTB.TextChanged += new System.EventHandler(this.currentCity_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(56, 245);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(149, 34);
            this.button2.TabIndex = 5;
            this.button2.Text = "Move package";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // UpdateStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 439);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.currentCityTB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.packageId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Back);
            this.Name = "UpdateStatus";
            this.Text = "UpdateStatus";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox packageId;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox currentCityTB;
        private System.Windows.Forms.Button button2;
    }
}