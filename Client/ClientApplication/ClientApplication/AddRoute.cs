﻿using ClientApplication.AdminOperations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class AddRoute : Form
    {
        AdminOperations.AdminServiceClient client = new AdminOperations.AdminServiceClient();

        public AddRoute()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminMenu adminMenuForm = new AdminMenu();
            adminMenuForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int packageId = Int32.Parse(packageIdTB.Text);
            package p = client.getPackage(packageId);
            String city = cityTB.Text;
            DateTime date = dateTimePicker.Value;

            route route = new route();
            route.city = city;
            route.package = p;
            route.time = date;

            client.addRoute(route);

            MessageBox.Show("Route added succesfully!");
        }
    }
}
