﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class RemovePackage : Form
    {
        AdminOperations.AdminServiceClient client = new AdminOperations.AdminServiceClient();

        public RemovePackage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminMenu adminMenuForm = new AdminMenu();
            adminMenuForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String packageId = packageIdTB.Text;
            int id = Int32.Parse(packageId);
            client.deletePackage(id);

            MessageBox.Show("Package removed!");
            
        }
    }
}
