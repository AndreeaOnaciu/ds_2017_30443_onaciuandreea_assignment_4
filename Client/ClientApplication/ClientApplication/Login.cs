﻿using ClientApplication.AdminOperations;
using ClientApplication.adminService;
using ClientApplication.ServiceReference1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;
using System.Windows.Forms;


namespace ClientApplication
{
  
    public partial class Login : Form
    {

        public Login()
        {
           
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String username = textBox1.Text;
            String password = textBox2.Text;

            AdminOperations.AdminServiceClient client = new AdminOperations.AdminServiceClient();
         
            AdminOperations.user user = client.login(username, password);
            
            if (user ==null)
            {
                MessageBox.Show("User not found !");
            }
            else if (user.role=="admin")
            {
                this.Hide();
                AdminMenu adminMenuForm = new AdminMenu();
                adminMenuForm.Show();

            }
            else
            {
                this.Hide();
                ClientMenu clientMenuForm = new ClientMenu(user.id);
                clientMenuForm.Show();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Register register = new Register();
            register.Show();
            this.Hide();
        }
    }
}
