﻿using ClientApplication.AdminOperations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class UpdateStatus : Form
    {
        AdminOperations.AdminServiceClient client = new AdminOperations.AdminServiceClient();
        ServiceReference2.WebService1SoapClient client2 = new ServiceReference2.WebService1SoapClient();
        public UpdateStatus()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String currentCity = packageId.Text;
            int intCurrCity = Int32.Parse(currentCity);
            currentCityTB.Text=client2.getCurrentCityOfPackage(intCurrCity);
            
        }

        private void currentCity_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int intCurrCity = Int32.Parse(packageId.Text);
            route r=client.getNextCity(intCurrCity);
            MessageBox.Show("The package status is updated now the package is in :"+r.city);
        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminMenu adminMenuForm = new AdminMenu();
           
           
            adminMenuForm.Show();
        }
    }
}
