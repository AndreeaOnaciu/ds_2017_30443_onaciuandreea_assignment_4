﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class SeePackages : Form
    {
        ServiceReference2.WebService1SoapClient client2 = new ServiceReference2.WebService1SoapClient();
        int userId;
        ServiceReference2.ArrayOfString[] packages;
        public SeePackages(int userId)
        {
            this.userId = userId;
            InitializeComponent();
            packages=client2.getPackagesForUser(userId);

            table.Columns.Add("Receiver", "Receiver");
            table.Columns.Add("Column","Name");
            table.Columns.Add("Description", "Description");
            table.Columns.Add("SenderCity", "SenderCity");
            table.Columns.Add("DestinationCity", "DestinationCity");

            for (int i=0;i<packages.Count();i++)
            {
                DataGridViewRow row = (DataGridViewRow)table.Rows[table.Rows.Add()];

                int nrcolumns = packages.ElementAt(i).Count();

                for (int j=1;j<nrcolumns;j++)
                {
                    //Console.WriteLine(packages.ElementAt(i).ElementAt(0));
                    row.Cells[j-1].Value=packages.ElementAt(i).ElementAt(j);
                }
                
            }
          
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClientMenu clientMenuForm = new ClientMenu(userId);
            clientMenuForm.Show();
        }

        private void table_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = table.CurrentCell.RowIndex;
            ServiceReference2.ArrayOfString package = packages.ElementAt(rowIndex);
            int packageId = Int32.Parse(packages.ElementAt(rowIndex).ElementAt(0));
            String currentCity=client2.getCurrentCityOfPackage(packageId);
            MessageBox.Show("The package is currently at " + currentCity+".");
        }

   
    }
}
