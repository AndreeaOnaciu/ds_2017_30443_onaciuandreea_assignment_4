﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class SearchPackage : Form
    {
        ServiceReference2.WebService1SoapClient client2 = new ServiceReference2.WebService1SoapClient();
        int userId;

        public SearchPackage(int userId)
        {
            this.userId = userId;       
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClientMenu clientMenuForm = new ClientMenu(userId);
            clientMenuForm.Show();
        }

        private void Search_Click(object sender, EventArgs e)
        {
            string nameOfPackages = packageNameTB.Text;
            

            ServiceReference2.ArrayOfString[] packages = client2.getPackagesByName(nameOfPackages);
            tabelGrid.Rows.Clear();
            tabelGrid.Columns.Clear();
            
            tabelGrid.Columns.Add("Receiver", "Receiver");
            tabelGrid.Columns.Add("Column", "Name");
            tabelGrid.Columns.Add("Description", "Description");
            tabelGrid.Columns.Add("SenderCity", "SenderCity");
            tabelGrid.Columns.Add("DestinationCity", "DestinationCity");

            
            for (int i = 0; i < packages.Count(); i++)
            {
                DataGridViewRow row = (DataGridViewRow)tabelGrid.Rows[tabelGrid.Rows.Add()];

                int nrcolumns = packages.ElementAt(i).Count();

                for (int j = 0; j < nrcolumns; j++)
                {

                    row.Cells[j].Value = packages.ElementAt(i).ElementAt(j);
                }

            }
        }
    }
}
