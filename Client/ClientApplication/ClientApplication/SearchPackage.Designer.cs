﻿namespace ClientApplication
{
    partial class SearchPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.packageNameTB = new System.Windows.Forms.TextBox();
            this.Search = new System.Windows.Forms.Button();
            this.tabelGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.tabelGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(507, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 27);
            this.button1.TabIndex = 0;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Package name:";
            // 
            // packageNameTB
            // 
            this.packageNameTB.Location = new System.Drawing.Point(171, 59);
            this.packageNameTB.Name = "packageNameTB";
            this.packageNameTB.Size = new System.Drawing.Size(144, 22);
            this.packageNameTB.TabIndex = 2;
            // 
            // Search
            // 
            this.Search.Location = new System.Drawing.Point(332, 57);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(122, 26);
            this.Search.TabIndex = 3;
            this.Search.Text = "Search";
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // tabelGrid
            // 
            this.tabelGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabelGrid.Location = new System.Drawing.Point(62, 111);
            this.tabelGrid.Name = "tabelGrid";
            this.tabelGrid.RowTemplate.Height = 24;
            this.tabelGrid.Size = new System.Drawing.Size(523, 150);
            this.tabelGrid.TabIndex = 4;
            // 
            // SearchPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 439);
            this.Controls.Add(this.tabelGrid);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.packageNameTB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "SearchPackage";
            this.Text = "SearchPackage";
            ((System.ComponentModel.ISupportInitialize)(this.tabelGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox packageNameTB;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.DataGridView tabelGrid;
    }
}