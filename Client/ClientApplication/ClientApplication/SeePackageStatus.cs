﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApplication
{
    public partial class SeePackageStatus : Form
    {
        int userId;
        public SeePackageStatus(int userId)
        {
            this.userId = userId;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClientMenu clientMenuForm = new ClientMenu(userId);
            clientMenuForm.Show();
        }
    }
}
