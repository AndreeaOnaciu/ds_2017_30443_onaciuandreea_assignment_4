﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data;

namespace ClientOPss
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        private const string ConnStr = "Driver={MySQL ODBC 3.51 Driver};Server=localhost;" +
            "Database=soaphw;uid=root;pwd=an15dr02#A;option=3";


        [WebMethod]
        public string register(string username, string password, string role, string name)
        {
            using (OdbcConnection con = new OdbcConnection(ConnStr))
            using (OdbcCommand cmd = new OdbcCommand("INSERT INTO user" +
                                    "(username,password,role,name) VALUES (?,?,?,?)", con))
            {
                cmd.Parameters.Add("@username", OdbcType.VarChar, 45).Value = username;
                cmd.Parameters.Add("@password", OdbcType.VarChar, 45).Value = password;
                cmd.Parameters.Add("@role", OdbcType.VarChar, 45).Value = role;
                cmd.Parameters.Add("@name", OdbcType.VarChar, 45).Value = name;
                con.Open();
                cmd.ExecuteNonQuery();
            }
            return "Register successfully!";
        }

        [WebMethod]
        public List<List<String>> getPackagesForUser(int userId)
        {
            List<List<String>> result = new List<List<string>>();
            string query = "SELECT id,receiver,name,description,senderCity,destinationCity FROM Package WHERE sender = ?";
            string receiver = "SELECT username FROM User WHERE id = ?";

            OdbcConnection myConnection = new OdbcConnection(ConnStr);
            OdbcConnection myConnection2 = new OdbcConnection(ConnStr);
            OdbcCommand myCommand = new OdbcCommand(query, myConnection);

            var parameter = myCommand.CreateParameter();

            parameter.Value = userId;
            myCommand.Parameters.Add(parameter);

            //myCommand.Parameters.Add("@senderId", OdbcType.Int).Value = userId;
            myConnection.Open();

            OdbcDataReader reader = myCommand.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    //Console.WriteLine(reader.GetString(0));
                    int id = reader.GetInt16(0);
                    int receiverId = reader.GetInt16(1);
                    string name = reader.GetString(2);
                    string description = reader.GetString(3);
                    string senderCity = reader.GetString(4);
                    string destinationCity = reader.GetString(5);

                    List<String> interm = new List<String>();
                    Console.WriteLine(id);
                    interm.Add(id.ToString());
                    interm.Add(receiverId.ToString());
                    interm.Add(name);
                    interm.Add(description);
                    interm.Add(senderCity);
                    interm.Add(destinationCity);

                    result.Add(interm);

                }
            }
            finally
            {
                reader.Close();
                myConnection.Close();
            }

            for (int i = 0; i < result.Count(); i++)
            {
                OdbcCommand myCommand2 = new OdbcCommand(receiver, myConnection2);
                var parameter2 = myCommand2.CreateParameter();
                parameter2.Value = Int32.Parse(result.ElementAt(i).ElementAt(1));
                myCommand2.Parameters.Add(parameter2);

                myConnection2.Open();
                OdbcDataReader reader2 = myCommand2.ExecuteReader();

                if (reader2.Read())
                {
                    result.ElementAt(i).RemoveAt(1);
                    result.ElementAt(i).Insert(1, reader2.GetString(0));
                }
                reader2.Close();
                myConnection2.Close();
            }
            return result;
        }


        [WebMethod]
        public List<List<String>> getPackagesByName(string name)
        {
            List<List<String>> result = new List<List<string>>();
            string query = "SELECT receiver,name,description,senderCity,destinationCity FROM Package WHERE name = ?";
            string receiver = "SELECT username FROM User WHERE id = ?";

            OdbcConnection myConnection = new OdbcConnection(ConnStr);
            OdbcConnection myConnection2 = new OdbcConnection(ConnStr);
            OdbcCommand myCommand = new OdbcCommand(query, myConnection);

            var parameter = myCommand.CreateParameter();

            parameter.Value = name;
            myCommand.Parameters.Add(parameter);

            //myCommand.Parameters.Add("@senderId", OdbcType.Int).Value = userId;
            myConnection.Open();

            OdbcDataReader reader = myCommand.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    //Console.WriteLine(reader.GetString(0));
                    int receiverId = reader.GetInt16(0);
                    string nameOfPackage = reader.GetString(1);
                    string description = reader.GetString(2);
                    string senderCity = reader.GetString(3);
                    string destinationCity = reader.GetString(4);

                    List<String> interm = new List<String>();

                    interm.Add(receiverId.ToString());
                    interm.Add(nameOfPackage);
                    interm.Add(description);
                    interm.Add(senderCity);
                    interm.Add(destinationCity);

                    result.Add(interm);

                }
            }
            finally
            {
                reader.Close();
                myConnection.Close();
            }

            for (int i = 0; i < result.Count(); i++)
            {
                OdbcCommand myCommand2 = new OdbcCommand(receiver, myConnection2);
                var parameter2 = myCommand2.CreateParameter();
                parameter2.Value = Int32.Parse(result.ElementAt(i).ElementAt(0));
                myCommand2.Parameters.Add(parameter2);

                myConnection2.Open();
                OdbcDataReader reader2 = myCommand2.ExecuteReader();

                if (reader2.Read())
                {
                    result.ElementAt(i).RemoveAt(0);
                    result.ElementAt(i).Insert(0, reader2.GetString(0));
                }
                reader2.Close();
                myConnection2.Close();
            }
            return result;
        }

        [WebMethod]
        public String getCurrentCityOfPackage(int id)
        {
            string query = "SELECT currentCity FROM Package WHERE id = ?";
            string city = null;

            OdbcConnection myConnection = new OdbcConnection(ConnStr);
            OdbcCommand myCommand = new OdbcCommand(query, myConnection);

            var parameter = myCommand.CreateParameter();

            parameter.Value = id;
            myCommand.Parameters.Add(parameter);

            myConnection.Open();

            OdbcDataReader reader = myCommand.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    city = reader.GetString(0);
                }

            }
            finally
            {
                reader.Close();
                myConnection.Close();
            }


            return city;

        }
    }
}
