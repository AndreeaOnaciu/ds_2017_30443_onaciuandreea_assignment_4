package services;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import dao.PackageDao;
import dao.RouteDao;
import dao.UserDao;
import entities.Package;
import entities.Route;
import entities.User;



@WebService(endpointInterface = "services.AdminService") 
public class AdminServiceImpl implements AdminService {
	
	private RouteDao routeDAO=new RouteDao();
	private PackageDao packageDAO=new PackageDao();
	private UserDao userDAO=new UserDao();
	
	public void savePackage(Package p){
		packageDAO.addPackage(p);
	}
	
	

	public void deletePackage(int id) {
		packageDAO.deletePackage(id);
	}
	
	
	public int addRoute(Route r){
		
		return routeDAO.addRoute(r);
	}
	
	public void editPackage(Package p){
		packageDAO.editPackage(p);
	}
	

	public Package getPackage(int id){
		return packageDAO.findPackage(id);
	}

	public List<Package> getPackages(){
		return packageDAO.findPackages();
	}
 
	public List<Route> getRoutes(){
		return routeDAO.findRoute();
	}

	public Route getNextCity(int packageId){
		List<Route> routeOfPackage=routeDAO.findRouteForPacakge(packageId);
		Package pack=packageDAO.findPackage(packageId);
				
		Route r=new Route();
		int i;
		
		if (pack.getSenderCity().equals(pack.getCurrentCity())) {
			r=routeOfPackage.get(0);
		}
		else if (pack.getDestinationCity().equals(pack.getCurrentCity())) {
			r=new Route(pack.getDestinationCity(),new Date(),pack);
		}
		else {
		for ( i=0;i<routeOfPackage.size();i++) {
			
			r=routeOfPackage.get(i);
			System.out.println(r.getCity()+" "+pack.getCurrentCity());
			if (r.getCity().equals(pack.getCurrentCity())) {
				if (i!=routeOfPackage.size()-1) {
				r=routeOfPackage.get(i+1);
				break;
				}
				else
				{
					r=new Route(pack.getDestinationCity(),new Date(),pack);
					break;
				}
			}
		}
		}
		
		
		pack.setCurrentCity(r.getCity());
		packageDAO.changeCurrentCity(pack);
		return r;
		
	}
	
	public User login(String username,String password){
		return userDAO.getUser(username, password);
	}

	public User getUser(String username) {
		// TODO Auto-generated method stub
		
		return userDAO.getUser(username);
	}
	
	
}
