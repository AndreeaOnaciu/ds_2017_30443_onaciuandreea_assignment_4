package services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import entities.Package;
import entities.Route;
import entities.User;

@WebService  
@SOAPBinding(style = Style.DOCUMENT)  
public interface AdminService {


	//add package -save package---
    @WebMethod 
	public void savePackage(Package p);
	
	//remove package -delete package---
    @WebMethod 
	public void deletePackage(int id);
	
	//register package for tracking aka add a route --
    @WebMethod 
	public int addRoute(Route r);
	
	//change tracking to true--
    @WebMethod 
	public void editPackage(Package p);
    @WebMethod 
	public Package getPackage(int id);
    @WebMethod 
	public List<Package> getPackages();
 
    @WebMethod 
	public List<Route> getRoutes();
    @WebMethod 
	public Route getNextCity(int packageId);
	
    @WebMethod 
	public User login(String username,String password);
    
    @WebMethod
    public User getUser(String username);
}
