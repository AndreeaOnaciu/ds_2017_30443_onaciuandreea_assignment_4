package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User implements Serializable{
	
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private int id;
		
		@Column(name="username")
	    private String username;
		
		@Column(name="password")
	    private String password;
		
		@Column(name="role")
	    private String role;
		
		@Column(name="name")
	    private String name;
	    
	    @ManyToOne
	    private Set<Package> packages=new HashSet<Package>(0);
		
		public User(String username, String password, String role, String name, Set<Package> packages) {
			super();
			this.username = username;
			this.password = password;
			this.role = role;
			this.name = name;
			this.packages = packages;
		}

	    public User() {
	    	
	    }
	    
		
		public String getRole() {
			return role;
		}

		public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public boolean isAdmin() {
	        return role.equals("admin");
	    }

	    public String getUsername() {
	        return username;
	    }

	    public void setUsername(String username) {
	        this.username = username;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

	    public void setRole(String role) {
	        this.role = role;
	    }
}
