package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="package")
public class Package implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@OneToMany
	@JoinColumn(name="sender")
	private User sender;
	
	@OneToMany
	@JoinColumn(name="receiver")
	private User receiver;
	
	@Column(name="description")
	private String description;
	
	@Column(name="senderCity")
	private String senderCity;
	
	@Column(name="destinationCity")
	private String destinationCity;
	
	@Column(name="tracking")
	private boolean tracking;
	
	@Column(name="name")
	private String name;
	

	@ManyToOne
	Set<Route> routes=new HashSet<Route>(0);
	
	@Column(name="currentCity")
	private String currentCity;

	
	public Package(User sender, User receiver, String description, String senderCity, String destinationCity,
			boolean tracking, String name, Set<Route> routes) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.description = description;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.tracking = tracking;
		this.name = name;
		this.routes = routes;
	}

	public Set<Route> getRoutes() {
		return routes;
	}

	public String getCurrentCity() {
		return currentCity;
	}

	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}

	public void setRoutes(Set<Route> routes) {
		this.routes = routes;
	}

	public Package() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
