package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entities.Package;
import entities.Route;

public class RouteDao {
	private SessionFactory factory;

    public RouteDao() {
    	 try {
             factory = new Configuration().configure().buildSessionFactory();
         } catch (HibernateException e) {
             e.printStackTrace();
         }
    }
    
    public int addRoute(Route route) {
		int routeId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		String message = null;
		System.out.println("first");
		try {
			tx = session.beginTransaction();
			System.out.println("save");
			routeId = (Integer) session.save(route);
			System.out.println("saving the route");
			System.out.println(route.getTime());
			route.setId(routeId);
			tx.commit();
		} catch (HibernateException e) {
			message = e.getMessage();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			System.out.println("finally");

			session.close();
		}
		return routeId;
	}
    
	@SuppressWarnings("unchecked")
	public List<Route> findRoute() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Route> packs = null;
		try {
			tx = session.beginTransaction();
			packs = session.createQuery("FROM Route").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return packs;
	}
	
	@SuppressWarnings("unchecked")
	public List<Route> findRouteForPacakge(int id)
	{
		Session session = factory.openSession();
		Transaction tx = null;
		List<Route> packs = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Route as r WHERE r.package.id = :id");
			query.setParameter("id", id);
			packs=query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return packs;
	}
	
	@SuppressWarnings("unchecked")
	public Route findRoute(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Route> packs= null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM  Route WHERE id = :id");
			query.setParameter("id", id);
			packs = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return packs != null && !packs.isEmpty() ? packs.get(0) : null;
	}
	
}
