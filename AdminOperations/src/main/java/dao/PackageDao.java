package dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entities.Package;

public class PackageDao {
	private SessionFactory factory;

    public PackageDao() {
    	 try {
             factory = new Configuration().configure().buildSessionFactory();
         } catch (HibernateException e) {
             e.printStackTrace();
         }
    }
    
    public int addPackage(Package pack) {
		int packageId = -1;
		
		pack.setCurrentCity(pack.getSenderCity());
		pack.setTracking(false);
		
		Session session = factory.openSession();
		Transaction tx = null;
		String message = null;
		try {
			tx = session.beginTransaction();
			packageId = (Integer) session.save(pack);
			pack.setId(packageId);
			tx.commit();
		} catch (HibernateException e) {
			message = e.getMessage();
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return packageId;
	}
    
	@SuppressWarnings("unchecked")
	public Package deletePackage(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Package> packs = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Package WHERE id = :id");
			query.setParameter("id", id);
			packs = query.list();
			if (packs.isEmpty())
				return null;
			session.delete(packs.get(0));
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return packs != null && !packs.isEmpty() ? packs.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Package> findPackages() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Package> packs = null;
		try {
			tx = session.beginTransaction();
			packs = session.createQuery("FROM Package").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return packs;
	}

	@SuppressWarnings("unchecked")
	public Package findPackage(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Package> packs= null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM  Package WHERE id = :id");
			query.setParameter("id", id);
			packs = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}

		} finally {
			session.close();
		}
		return packs != null && !packs.isEmpty() ? packs.get(0) : null;
	}
	
	@SuppressWarnings("deprecation")
	@Transactional
	public int editPackage(Package pack) {
		Session session = factory.openSession();

		Transaction tr = session.beginTransaction();
		Package p = (Package) session.get(Package.class, pack.getId());
		p.setTracking(true);
		tr.commit();
		session.update(p);

		session.close();
		return p.getId();
	}
	
	@SuppressWarnings("deprecation")
	@Transactional
	public int changeCurrentCity(Package pack) {
		Session session = factory.openSession();

		Transaction tr = session.beginTransaction();
		Package p = (Package) session.get(Package.class, pack.getId());
		p.setCurrentCity(pack.getCurrentCity());
		tr.commit();
		session.update(p);

		session.close();
		return p.getId();
	}
	

}
